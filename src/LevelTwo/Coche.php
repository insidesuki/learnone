<?php
declare(strict_types = 1);

namespace Insidesuki\Coche\LevelTwo;
class Coche
{

	private int    $tipo;
	private string $color;


	public function __construct(string $color)
	{
		$this->tipo  = 1;
		$this->color = $color;
	}

	public function getColor(): string
	{
		return $this->color;
	}

	public function getTipo(): int
	{
		return $this->tipo;
	}


}