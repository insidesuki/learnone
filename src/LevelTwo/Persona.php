<?php
declare(strict_types = 1);

namespace Insidesuki\Coche\LevelTwo;
class Persona
{

	private string $dni;
	private string $nombre;
	private string $apellido1;
	private ?string $apellido2;
	private string $sexo;
	private int    $edad;

	public function __construct(
		string $dni,
		string $nombre,
		string $apellido1,
		string $sexo,
		?string $apellido2 = null

	)
	{
		$this->dni       = $dni;
		$this->nombre    = $nombre;
		$this->apellido1 = $apellido1;
		$this->sexo      = $sexo;
		$this->apellido2 = $apellido2;
	}


	public function cambiarEdad(int $edad): void
	{
		$this->edad = $edad;

	}

	public function edad(): int
	{
		return $this->edad;
	}


	public function cambiarDeSexo(string $sexo): void
	{

		$this->sexo = $sexo;
	}


	public function dni(): string
	{
		return $this->dni;
	}

	public function nombre(): string
	{
		return $this->nombre;
	}

	public function apellido1(): string
	{
		return $this->apellido1;
	}

	public function apellido2(): ?string
	{
		return $this->apellido2;
	}

	public function sexo(): string
	{
		return $this->sexo;
	}


}

