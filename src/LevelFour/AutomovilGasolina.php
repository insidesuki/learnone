<?php
declare(strict_types = 1);

namespace Insidesuki\Coche\LevelFour;

use Insidesuki\Coche\LevelFour\Exceptions\AutoSinGasolinaException;
use Insidesuki\Coche\LevelFour\Exceptions\BajoNivelGasolinaException;

class AutomovilGasolina extends AbstractAutomovil
{

	public const CONSUMO_ARRANCAR = 10;
	private int    $nivelGasolina = 0;


	public function arrancar(): void
	{

		if($this->nivelGasolina === 0) {
			throw new AutoSinGasolinaException();
		}
		if($this->nivelGasolina < self::CONSUMO_ARRANCAR) {
			throw new BajoNivelGasolinaException();
		}
		$this->encendido     = true;
		$this->nivelGasolina -= self::CONSUMO_ARRANCAR;

	}

	public function repostar(int $litros): void
	{

		$this->nivelGasolina += $litros;

	}

	public function encendido(): bool
	{
		return $this->encendido;
	}


	public function nivelGasolina(): int
	{
		return $this->nivelGasolina;
	}


}