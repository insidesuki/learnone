<?php
declare(strict_types = 1);

namespace Insidesuki\Coche\LevelFour;
abstract class AbstractAutomovil
{

	protected string $marca;
	protected string $modelo;
	protected string $color;
	protected bool   $encendido;


	public function __construct(string $marca, string $modelo, string $color)
	{
		$this->marca  = $marca;
		$this->modelo = $modelo;
		$this->color  = $color;
		$this->encendido = false;
	}

	public function marca(): string
	{
		return $this->marca;
	}

	public function modelo(): string
	{
		return $this->modelo;
	}

	public function color(): string
	{
		return $this->color;
	}




}