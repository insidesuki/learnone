<?php
declare(strict_types = 1);

namespace Insidesuki\Coche\LevelThree;

use Insidesuki\Coche\LevelFour\Exceptions\AutoSinGasolinaException;
use Insidesuki\Coche\LevelFour\Exceptions\BajoNivelGasolinaException;

class Automovil
{

	public const CONSUMO_ARRANCAR = 10;

	private string $marca;
	private string $modelo;
	private string $color;
	private bool   $encendido;
	private int    $nivelGasolina = 0;


	public function __construct(string $marca, string $modelo, string $color)
	{
		$this->marca     = $marca;
		$this->modelo    = $modelo;
		$this->color     = $color;
		$this->encendido = false;
	}


	public function arrancar(): void
	{

		if($this->nivelGasolina === 0) {
			throw new AutoSinGasolinaException();
		}
		if($this->nivelGasolina < self::CONSUMO_ARRANCAR) {
			throw new BajoNivelGasolinaException();
		}
		$this->encendido     = true;
		$this->nivelGasolina -= self::CONSUMO_ARRANCAR;

	}

	public function repostar(int $litros): void
	{

		$this->nivelGasolina += $litros;

	}

	public function encendido(): bool
	{
		return $this->encendido;
	}


	public function marca(): string
	{
		return $this->marca;
	}

	public function modelo(): string
	{
		return $this->modelo;
	}

	public function color(): string
	{
		return $this->color;
	}

	public function nivelGasolina(): int
	{
		return $this->nivelGasolina;
	}


}