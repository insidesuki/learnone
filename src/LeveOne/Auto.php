<?php
declare(strict_types = 1);

namespace Insidesuki\Coche\LeveOne;
class Auto
{

	public    $tipo;
	public  $color;
	private   $motor;


	public function __construct()
	{

		$this->tipo  = 'camioneta';
		$this->color = 'rojo';

	}

	public function getColor()
	{
		return $this->color;
	}


}