<?php

namespace LevelThree;

use Insidesuki\Coche\LevelFour\AutomovilGasolina;
use Insidesuki\Coche\LevelFour\Exceptions\AutoSinGasolinaException;
use Insidesuki\Coche\LevelFour\Exceptions\BajoNivelGasolinaException;
use PHPUnit\Framework\TestCase;

/**
 * @property AutomovilGasolina $polo
 */
class AutomovilTest extends TestCase
{


	public function setUp(): void
	{
		$this->polo = new AutomovilGasolina('vw', 'polo', 'negro');

	}

	public function testAutomovilWasCreated()
	{

		$this->assertInstanceOf(AutomovilGasolina::class, $this->polo);
		$this->assertSame('vw', $this->polo->marca());
		$this->assertSame('polo', $this->polo->modelo());
		$this->assertSame('negro', $this->polo->color());
		$this->assertFalse($this->polo->encendido());

	}


	public function testFailWhenNoGas()
	{

		$this->expectException(AutoSinGasolinaException::class);
		$this->polo->arrancar();
		$this->assertTrue($this->polo->encendido());

	}


	public function testOkRepostar()
	{

		$this->polo->repostar(20);
		$this->assertSame(20, $this->polo->nivelGasolina());
		$this->polo->repostar(30);
		$this->assertSame(50, $this->polo->nivelGasolina());

	}

	public function testOkCarOn()
	{

		$this->expectException(BajoNivelGasolinaException::class);


		$litrosRepostar = 5;
		$this->polo->repostar($litrosRepostar);
		$this->polo->arrancar();
		$this->assertTrue($this->polo->encendido());
		$consumoArrancar = $litrosRepostar - AutomovilGasolina::CONSUMO_ARRANCAR;
		$this->assertSame($consumoArrancar, $this->polo->nivelGasolina());

	}


}
