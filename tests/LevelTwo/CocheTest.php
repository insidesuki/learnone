<?php
declare(strict_types = 1);
namespace LevelTwo;

use Insidesuki\Coche\LevelTwo\Coche;
use PHPUnit\Framework\TestCase;

class CocheTest extends TestCase
{

	public function testCocheVerde(){

		$miCocheVerde = new Coche('verde');
		$this->assertSame('verde',$miCocheVerde->getColor());
		$this->assertSame(1,$miCocheVerde->getTipo());


	}

	public function testCocheRojo(){

		$miCocheRojo = new Coche('rojo');
		$this->assertSame('rojo',$miCocheRojo->getColor());


	}


}
