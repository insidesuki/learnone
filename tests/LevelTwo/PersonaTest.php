<?php

namespace LevelTwo;

use Insidesuki\Coche\LevelTwo\Persona;
use PHPUnit\Framework\TestCase;

class PersonaTest extends TestCase
{

	public function testPersona(){

		$diego = new Persona('dni123','diego','rojas','m');
		$this->assertSame('diego',$diego->nombre());
		$this->assertNull($diego->apellido2());


	}

	public function testPersonaConApellido(){

		$diego = new Persona('dni123','diego','rojas','m','rrr');
		$this->assertSame('diego',$diego->nombre());
		$this->assertSame('rrr',$diego->apellido2());


	}

	public function testCambiarSexo(){
		$diego = new Persona('dni123','diego','rojas','m');
		$this->assertSame('m',$diego->sexo());

		$diego->cambiarDeSexo('f');
		$this->assertSame('f',$diego->sexo());

	}

}
